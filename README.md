# What

## Dependencies: 
* Python3 - `sudo s/apt-get/yum/dnf/<whatever your distro requires install python`
* PyGame - `sudo pip3 install pygame`

Note: This project assumes a linux environment. There's no reason this code should not work on windows with the properly installed dependencies, but it has not been tested on windows. 


## How To
For now `python boxing_combos.py` will run the app. 

To change the interval change the interval variable, to the desired setting. 

Ctrl+C will terminate the script. 

Note: This project is a big work in progress. 

Todo: 
1. Threading for timer. 
2. Thread for a slider to change interval. 
3. Nicer colors. 
4. Nicer flash to indicate different combo. 
5. Difficulty levels from one punch, to two punch combows, and up.
6. Threaded kill button. 

## Output: 

This application opens a window that displays the text `jab:1, or right-uppercut:5` for punches. It may prove to be useful for hobby boxers trying to train to think when punching, kinda like Lomachenko's training. 


Note: The current script only does single punches. They range from 1 to 6, so there will be duplicate punches. The colors of the font attempt to show the newest punch. 
