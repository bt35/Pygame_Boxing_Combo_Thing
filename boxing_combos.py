from time import sleep
from random import randint, shuffle
import pygame 
from pygame.locals import * 

pygame.init()

myfont = pygame.font.SysFont("monospace", 100, bold=True)
xmax = 1200
ymax = 500
WINSIZE = [xmax,ymax]
center = [(xmax/2), (ymax/2)]

screen = pygame.display.set_mode((xmax,ymax),0,24)
white = 200,240,200
black = 20,20,40

interval = 2

global last_punch
nums = list(range(0,5))

#Easy Mode... learning mode. 
combos = {'jab':1, 'right-cross':2, 'left-hook':3, 'right-hook':4, 'right-uppercut':5, 'left-upper-cut':6}

def draw(screen):
    global last_punch # Set global for last puch.
    # random nubmer between 0 and 5. 
    nums = list(range(0,6))
    shuffle(nums)
    i = nums.pop()
    print(i)
    punch = str(list(combos.keys())[i]) + ':' + str(list(combos.values())[i])
    label = myfont.render(punch, 1, (randint(1,150),randint(1,150),randint(1,150)))
    screen.blit(label,(10,100))
    
def main():
    pygame.init()
    sceen = pygame.display.set_mode(WINSIZE)
    pygame.display.set_caption('Boxing Combo Trainer')
    screen.fill(white)
    while True:
        draw(screen)
        sleep(interval)
        pygame.display.update()
        screen.fill(white)
if __name__ == '__main__':
    main()
